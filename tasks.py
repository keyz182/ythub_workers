import logging
import docker
import os
import json
import tempfile
import threading
import subprocess as sp
from celery import shared_task, Task
import base64

__UPLOADS__ = "uploads/"
BASE_IMAGE = 'ytproject/yt-devel'
BASE_IMAGE_TAG = 'hublaunch'
CONTAINER_STORAGE = "/tmp/containers.json"
CONTAINER_VERSION = '1.0.0'

IRODS_DATADIR = "/NDS/home/kacperk/data/"
MOUNTS_DIR = '/tmp/mounts/'
name = "xarthisius"
workdir = "results"
lock = threading.Lock()

RUNMODE = "django"

try:
    from backend import celeryconfig
    from celery import Celery
    celery = Celery()
    celery.config_from_object(celeryconfig)
    RUNMODE = "tcelery"
except ImportError:
    logging.debug("Running in Django, not re-loading celery.")


class ContainerException(Exception):

    """
    There was some problem generating or launching a docker container
    for the user
    """
    pass


def get_image(image_name, image_tag):
    # import celery.app.control
    # app = celery.app.App()
    # i = app.control.inspect()
    # stats = i.stats()
    # q = i.active_queues()

    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    # TODO catch ConnectionError - requests.exceptions.ConnectionError
    for image in dcli.images():
        if image['Repository'] == image_name and image['Tag'] == image_tag:
            return image
    raise ContainerException("No image found")
    return None


def remember_container(name, containerid, tmpdir=None):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    payload = {'id': containerid, 'mount': tmpdir}
    container_store = CONTAINER_STORAGE
    with lock:
        if not os.path.exists(container_store):
            containers = {'version': CONTAINER_VERSION}
        else:
            containers = json.load(open(container_store, 'rb'))
            if "version" not in containers \
                    or containers["version"] != CONTAINER_VERSION:
                os.remove(container_store)
            containers = {'version': CONTAINER_VERSION}
        if name in containers:
            containers[name].append(payload)
        else:
            containers[name] = [payload]
        json.dump(containers, open(container_store, 'wb'))


class WorkerSpecificTask(Task):
    abstract = True

    @property
    def hostname(self):
        if RUNMODE == 'tcelery':
            return ''
        try:
            from celery import current_task
            hostname=current_task.request.hostname
            return hostname
        except:
            return ''


@shared_task(base=WorkerSpecificTask)
def create_job_docker(*args, **kwargs):
    mountpoint = '/blah'
    yt_data_dir = '/mnt/yt_data'
    tmpdir = None
    binds = {}
    queue = create_job_docker.hostname
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    if kwargs:
        logging.info('kwargs: %s' % kwargs)
        file = kwargs['script']
        filename = kwargs['scriptname']
        image_name = kwargs["image_name"]
        image_tag = kwargs["image_tag"]
        if "data" in kwargs:
            tmpdir = prepare_dir_irods(kwargs["data"])
            binds.update({tmpdir: {"bind": yt_data_dir}})
            tmpdir = os.path.join(tmpdir, kwargs["data"])

    with tempfile.NamedTemporaryFile(suffix='.py', mode='w', delete=False) as auxf:
        if(isinstance(file, basestring)):
            auxf.write(file)
        bind_src, script_name = os.path.split(auxf.name)

    binds.update({bind_src: {"bind": mountpoint}})
    image = get_image(image_name, image_tag)
    mountpoint = '/blah'
    cont = dcli.create_container(
        image['Id'],
        ['python', '-u', os.path.join(mountpoint, script_name)],
        hostname="{user}box".format(user=name.split('-')[0]),
        ports=[8888, 4200],
        working_dir=workdir,
        volumes=[mountpoint, yt_data_dir]
    )
    remember_container(name, cont.get('Id'), tmpdir=tmpdir)
    # check_memory()
    dcli.start(cont['Id'], binds=binds, publish_all_ports=True)
    return json.dumps(
        {'Id': cont['Id'],
         'script': filename,
         'worker': queue})


@shared_task(base=WorkerSpecificTask)
def get_job_state_docker(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    if args:
        logging.info('args: %s' % args)
    if kwargs:
        logging.info('kwargs: %s' % kwargs)

    cont_id = kwargs['_query']['cont_id'][0]
    try:
        info = dcli.inspect_container(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"State": "UNKNOWN", "error": e.explanation})
    state = info['State']
    # {u'Pid': 0, u'Paused': False, u'Running': False,
    #  u'FinishedAt': u'2014-10-13T18:13:18.199756378Z',
    #  u'Restarting': False, u'StartedAt': u'2014-10-13T17:13:15.703794407Z',
    #  u'ExitCode': 0}
    if state["Running"]:
        return json.dumps({"State": "RUNNING",
                           "StartedAt": state["StartedAt"]})
    if state['ExitCode'] != 0:
        return json.dumps({"State": "FAILED", "ExitCode": state['ExitCode']})
    return json.dumps({"State": "COMPLETED"})


@shared_task(base=WorkerSpecificTask)
def get_job_stdout_docker(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    cont_id = kwargs['_query']['cont_id'][0]
    cont_id = cont_id.replace('"', '').replace("'", '')
    try:
        logs = dcli.logs(cont_id, stdout=True, stderr=True,
                         stream=False, timestamps=False)
    except docker.errors.APIError as e:
        return json.dumps({"stdout": None, "error": e.explanation})
    return json.dumps({"stdout": logs})


@shared_task(base=WorkerSpecificTask)
def get_job_metric_docker(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    cont_id = kwargs['_query']['cont_id'][0]
    cont_id = cont_id.replace('"', '').replace("'", '')
    try:
        data = dcli.inspect_container(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps(data)


@shared_task(base=WorkerSpecificTask)
def get_job_changes_docker(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    cont_id = kwargs['_query']['cont_id'][0]
    cont_id = cont_id.replace('"', '').replace("'", '"')
    try:
        data = dcli.diff(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps(data)


@shared_task(base=WorkerSpecificTask)
def get_job_filepath_docker(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    cont_id = kwargs['_query']['cont_id'][0]
    cont_id = cont_id.replace('"', '').replace("'", '"')
    path = kwargs['_query']['path'][0]
    try:
        raw = dcli.copy(cont_id, path)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps({'file': base64.b64encode(raw.read())})


@shared_task
def get_list_data_irods(*args, **kwargs):
    data_dirs = []
    for idir in sp.check_output(['ils', IRODS_DATADIR]).split('\n'):
        if idir.startswith('  C- '):
            data_dirs.append(idir.split(IRODS_DATADIR)[-1])
    return json.dumps({"data_dirs": data_dirs})


@shared_task
def umount_tmpdir_fuse(*args, **kwargs):
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)

    container_store = CONTAINER_STORAGE
    with lock:
        if os.path.exists(container_store):
            containers = json.load(open(container_store, 'rb'))
        else:
            return

    cwd = os.getcwd()
    os.chdir(MOUNTS_DIR)
    for user_containers in containers.values():
        for container in user_containers:
            try:
                info = dcli.inspect_container(container["id"])
                state = info["State"]
                if container["mount"] is None:
                    continue
                if not state["Running"] and not state["Paused"] \
                        and os.path.ismount(container["mount"]):
                    status = sp.call("fusermount -u " + container["mount"],
                                     shell=True)
                    if status == 0:
                        os.removedirs(
                            os.path.relpath(container["mount"], MOUNTS_DIR)
                        )
            except docker.errors.APIError as e:
                pass
    os.chdir(cwd)


def prepare_dir_irods(data):
    if not os.path.isdir(MOUNTS_DIR):
        os.mkdir(MOUNTS_DIR)
    tmpdir = tempfile.mkdtemp(prefix=MOUNTS_DIR)
    os.mkdir(os.path.join(tmpdir, data))
    cmd = 'icd ' + os.path.join(IRODS_DATADIR, data) + ' && ' + \
        'irodsFs -o allow_other ' + os.path.join(tmpdir, data)
    status = sp.call(cmd, shell=True)
    return tmpdir
